# Hubzilla Project Page

## Repo structure

The self-contained version of the page should be in `./sandbox` where it is easy to view the site and make rapid iterations. Those changes should ideally be integrated into the webpage elements that can be used in the Webpages tool to create the site as one of a channel's webpages. 

This repo is formatted for compatibility with the [Hubsites plugin](https://gitlab.com/zot/hubsites), which allows you to easily clone and import webpage elements through an interactive dialog. It makes development and updating this project page easier than using the Webpages app directly.

## What are webpage elements?

Hubzilla provides a set of tools to create webpages in a modular and collaborative way, allowing you to generate feature-rich, identity aware pages that can take full advantage of the [Hubzilla network](https://github.com/redmatrix/hubzilla).

## How to use webpage elements

Copy the contents of each of the folder into elements of the type indicated by the folder name. For example, create a **Block** element called `home-content` using `blocks/home-content.txt`

### Create the blocks

![image](images/blocks.png)

### Create the two menus for left and right

To import the menus from the encoded string in the menu files, start composing a new post and paste the string into the composition box. Click the preview post button to generate an installation link. Click the link to install the menu element. You do not need to submit the post. 

![image](images/editpost.png)

![image](images/menus.png)

### Create the layout

![image](images/layouts.png)

### Create a page using the layout

![image](images/pages.png)
